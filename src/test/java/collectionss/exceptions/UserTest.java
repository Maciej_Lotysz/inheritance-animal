package collectionss.exceptions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserTest {

    private static User user;

    @Test
    public void shouldTrowExceptionWhenLParamIsNull() {
        //given and when

        final Executable userCreation = () -> User.create
                ("kuba1",
                null,
                "Jakub",
                "kuba@gmail.com"
        );
        assertThrows(NullPointerException.class, userCreation);
    }

    @Test
    public void shouldTrowExceptionWhenLLoginIsTooShort() {
        //given and when

        final Executable userCreation = () -> User.create
                ("kuba1",
                        "admin12345",
                        "Jakub",
                        "kuba@gmail.com"
                );
        assertThrows(IllegalArgumentException.class, userCreation);
    }

}