package collectionss.sets;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class MainSets {

    public static void main(String[] args) {

        Set<Glasses> glassesSet = new HashSet<>();
        Glasses glasses1 = new Glasses("RayBan", "Aviator", 2599);
        Glasses glasses2 = new Glasses("New Balance", "Lupetto", 799);
        Glasses glasses3 = new Glasses("RayBan", "Aviator", 2599);
        Glasses glasses4 = new Glasses("Polaroid", "Classic", 1499);
        Glasses glasses5 = new Glasses("Woodfi Timber", "Blue Monday", 1759);
        Glasses glasses6 = new Glasses("Michael Kors", "Sunshine Moon", 2399);
        Glasses glasses7 = new Glasses("Fendi", "TrendyMF", 959);
        Glasses glasses8 = new Glasses("Bulgari", "Ekstravaganzza", 4599);
        Glasses glasses9 = new Glasses("Fred", "Tiger&Kobra", 499);
        Glasses glasses10 = new Glasses("Woodfi Timber", "Blue Monday", 1759);

        glassesSet.add(glasses1);
        glassesSet.add(glasses2);
        glassesSet.add(glasses3);
        glassesSet.add(glasses4);
        glassesSet.add(glasses5);
        glassesSet.add(glasses6);
        glassesSet.add(glasses7);
        glassesSet.add(glasses8);
        glassesSet.add(glasses9);
        glassesSet.add(glasses10);

        for (Glasses glasses : glassesSet) {
            System.out.println(glasses);
        }
        System.out.println();

        glassesSet
                .stream()
                .sorted(Comparator.comparingInt(Glasses::getPrice))
                .forEach(System.out::println);
    }
}
