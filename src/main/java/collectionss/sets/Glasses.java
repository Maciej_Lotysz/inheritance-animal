package collectionss.sets;

import lombok.Data;

@Data
public class Glasses {

    private final String mark;
    private final String Model;
    private final Integer price;

}
