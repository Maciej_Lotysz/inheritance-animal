package collectionss;

import java.util.*;

public class ListExample {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        Random r = new Random();

        Integer randomNumber;
        for (int i = 0; i <= 20; i++) {
           randomNumber = r.nextInt(11);
           list.add(randomNumber);
        }

        System.out.println(list);

//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i) % 2 == 0) {
//                list.remove(i);
//                i--;
//            }
//        }

//        Iterator<Integer> it = list.iterator();
//        while (it.hasNext()){
//            if (it.next() % 2 == 0) {
//                it.remove();
//            }
//        }
//        System.out.println(list);

        //lambda
        list.removeIf(el -> el % 2 ==0);
        System.out.println(list);
    }
}
