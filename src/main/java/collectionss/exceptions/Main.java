package collectionss.exceptions;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {

       try {
           User user = User.create(
                   "ad34465", "TwojaStaraTTwójStary",
                   "Jakub",
                   "admin@gmail.com");
           System.out.println(user);
       } catch (NullPointerException e){
           System.out.println("Nie udało się stworzyć Użytkownika. " + e.getMessage());
       } catch (IllegalArgumentException e) {
           System.out.println("Invalid argument. " + e.getMessage());
       }finally {
           System.out.println("Koniec");
       }
       }




}