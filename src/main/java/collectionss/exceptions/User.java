package collectionss.exceptions;

import lombok.*;

import java.time.LocalDateTime;

@EqualsAndHashCode
@ToString
@Getter
public class User {

    private final String login;
    private final String password;
    private final String firstName;
    private final String email;
    private final LocalDateTime creationDate;

    private User(String login, String password, String firstName, String email, LocalDateTime creationDate) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.email = email;
        this.creationDate = creationDate;
    }

    public static User create(String login, String password, String firstName, String email) {
        if (login == null || password == null || firstName == null || email == null) {
            throw new NullPointerException("Pole nie może być puste!");
        }
        if (login.length() < 6) {
            throw new IllegalArgumentException("Login jest za krótki!");
        }
        LocalDateTime creationDate = LocalDateTime.now();

        return new User(login, password, firstName, email, creationDate);
    }
}
