package collectionss.maps;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class StudentMain {

    public static void main(String[] args) {

        Map<Student, List<Integer>> map = new HashMap<>();

        Student s1 = new Student("Pioter", "Cebula", "polski");
        Student s2 = new Student("Adaś", "Niezgódka", "polski");
        Student s3 = new Student("Michał", "Czerczesow", "polski");
        Student s4 = new Student("Anastazja", "Popov", "polski");
        Student s5 = new Student("Klaudia", "van Dijk", "polski");
        Student s6 = new Student("Martyna", "Wojciechowska", "polski");

        List<Integer> gradesS1 = getGrades();
        List<Integer> gradesS2 = getGrades();
        List<Integer> gradesS3 = getGrades();
        List<Integer> gradesS4 = getGrades();
        List<Integer> gradesS5 = getGrades();
        List<Integer> gradesS6 = getGrades();

        map.put(s1, gradesS1);
        map.put(s2, gradesS2);
        map.put(s3, gradesS3);
        map.put(s4, gradesS4);
        map.put(s5, gradesS5);
        map.put(s6, gradesS6);

        for (Map.Entry<Student, List<Integer>> entry : map.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

    }

    @NotNull
    private static List<Integer> getGrades() {
        List<Integer> gradesS1 = new ArrayList<>();
        Random r = new Random();
        int grades;
        for (int i = 1; i < 7 ; i++) {
            grades = 1 + r.nextInt(6);
            gradesS1.add(grades);
        }
        return gradesS1;
    }
}
