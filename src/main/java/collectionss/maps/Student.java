package collectionss.maps;

import lombok.*;

@Data
@AllArgsConstructor
public class Student {

    private String firstName;
    private String lastName;
    private String mainLanguage;
}
