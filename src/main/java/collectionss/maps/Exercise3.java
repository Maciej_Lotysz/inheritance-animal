package collectionss.maps;

import java.util.*;

public class Exercise3 {

    public static void main(String[] args) {

        Map<Long, String> namesMap = new HashMap<>();

        namesMap.put(1L, "Anastazja");
        namesMap.put(2L, "Amanda");
        namesMap.put(3L, "Adelajda");
        namesMap.put(4L, "Ambroży");
        namesMap.put(5L, "Antoni");
        namesMap.put(6L, "Abelard");
        namesMap.put(7L, "Jan");
        namesMap.put(8L, "Marta");
        namesMap.put(9L, "Bartosz");
        namesMap.put(10L, "Jan");
        namesMap.put(11L, "Jan");

        for (String value : namesMap.values()) {
            if (value.toUpperCase().charAt(0) == 'A') {
                System.out.println(value);
            }
        }
        System.out.println();

        for (Map.Entry<Long, String> entry : namesMap.entrySet()) {
            if (entry.getValue().equals("Jan")) {
                System.out.println(entry.getKey());
            }
        }

    }



}
