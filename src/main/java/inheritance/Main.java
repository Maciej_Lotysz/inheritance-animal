package inheritance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Animal dog1 = new Dog("Burek", 10, "Kundel", "Mieszano-łaciaty");
        Animal dog2 = new Dog("Anteya", 5, "Cane Corso", "Czarno-brązowe");
        Animal dog3 = new Dog("Coco", 7, "Cane Corso", "Czarno-brązowe");
        Animal cat1 = new Cat("Mruczysław", 5, "Dachowiec");
        Animal cat2 = new Cat("Lord Mruk", 10, "Grumpy Cat");
        Animal cat3 = new Cat("Humans Slayer", 8, "Kot Norweski");

        cat1.voice();
        dog1.voice();

        System.out.println();
        System.out.println(dog2);
        System.out.println();

        List<Animal> animalList = new ArrayList<>();
        animalList.add(dog1);
        animalList.add(dog2);
        animalList.add(dog3);
        animalList.add(cat1);
        animalList.add(cat2);
        animalList.add(cat3);

        displayAnimals(animalList);

    }

    private static void displayAnimals(final List<Animal> animalList) {
        animalList
                .stream()
                .sorted(Comparator.comparing(Animal::getName))
                .map(Animal::getName)
                .distinct()
                .forEach(System.out::println);
    }

}
