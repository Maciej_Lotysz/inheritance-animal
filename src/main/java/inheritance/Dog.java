package inheritance;

public class Dog extends Animal {

    private String color;

    public Dog(String name, int age, String race, String color) {
        super(name, age, race);
        this.color = color;
    }

    @Override
    void voice() {
        System.out.println("Dogs barks: Wow! wow! wow! (in polish edition the piesełs makes hau! hau! hau!)");
    }
}
