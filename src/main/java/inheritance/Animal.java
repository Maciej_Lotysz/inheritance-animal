package inheritance;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public abstract class Animal implements Creature{

    protected String name;
    protected int age;
    protected String race;

    abstract void voice();

}