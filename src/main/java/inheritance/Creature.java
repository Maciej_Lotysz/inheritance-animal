package inheritance;

public interface Creature {

    String getName();
    int getAge();
    String getRace();
}
