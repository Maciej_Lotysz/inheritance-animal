package inheritance;

public class Cat extends Animal{

    public Cat(String name, int age, String race) {
        super(name, age, race);
    }

    @Override
    void voice() {
        System.out.println("Cats meows: meow, meow, meow (in polish edition the kotełs makes miau, miau, miau!)");

    }
}
